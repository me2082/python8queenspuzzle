def checkPlacement(chessBoard,y,x,sizeOfBoard): #Check the wished queen placement position

    for i in range(sizeOfBoard): #Loops Through the board

        if chessBoard[y][i]==1: #Checks for queens on the y coord

            return False #If queen is in the way

        if chessBoard[i][x]==1: #Checks for queens on the x coord

            return False #If queen is in the way

        for j in range(sizeOfBoard): #Loops Through the board

            if chessBoard[i][j]==1: #Finds Queen at pos

                if (i - y) == (j - x): #Check if found queen is diagonally to placed queen

                    return False #If queen is in the way

    return True #Placement is valid