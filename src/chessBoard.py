import numpy as np
import solve as solver

#My OCD hates that i cannot move everything after the "=" to the next line
chessBoard=[
      [0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0],
      [0,0,0,0,0,0,0,0]]

solvedBoard = solver.placeQueens(chessBoard) #Calls solve
print(np.matrix(solvedBoard)) #Prints Solution to the console