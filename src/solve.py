from checkPlacement import checkPlacement #Imports the checkPlacement Method


def placeQueens(chessBoard):
    
    sizeOfBoard=len(chessBoard) #Gets the size of the board 
    
    for y in range(sizeOfBoard): #Goes through the board

        for x in range(sizeOfBoard): #Goes through the board

            if chessBoard[y][x]==0: #If there is an empty spot for a queen

                if checkPlacement(chessBoard,y,x,sizeOfBoard): #Calls checkPlacement to check if placement is valid

                    chessBoard[y][x]=1 #Places queen in chessboard
                    placeQueens(chessBoard) #Calls itselfs

                    if sum(sum(a) for a in chessBoard)==sizeOfBoard: #Checks if we have placed all queens

                        return chessBoard #We have completed the 8QueensPuzzle

                    chessBoard[y][x]=0 #We dont have all the queens on the board so we are going to backtrack
